This is the translation for chapter 2 of Sweet Gap.

The pdf document is located [here](https://tosiaki.gitlab.io/sweet-gap-chapter-2/main.pdf).

The main document to edit is main.tex. GitLab has an online IDE that allows you to edit the document online directly. Once you finalize your edit, it may take up to 3 minutes for the CI/CD pipeline to update the pdf.

There may eventually be a guide on how to preview the document locally so that there is no need to wait for the pipeline to recompile the document.
