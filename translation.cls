\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{translation}[2020/07/17 Translations Document Class]

\LoadClass{article}

\RequirePackage{fontspec}
\RequirePackage{indentfirst}
\RequirePackage{newunicodechar}
\RequirePackage{xeCJK}
\RequirePackage{ruby}
\RequirePackage[margin=0.1in]{geometry}
\RequirePackage[table]{xcolor}
\RequirePackage{xltabular}
\RequirePackage{booktabs}

\setCJKmainfont{meiryo.ttc}
\newfontfamily\meiryo{meiryo.ttc}
\newfontfamily\greekfont{OldStandard-Regular.otf}

\renewcommand{\rubysize}{0.7}

\def\arraystretch{1.5}

\newunicodechar{♥}{{\meiryo♥}}
\newunicodechar{♡}{{\meiryo♡}}
\newunicodechar{★}{{\meiryo★}}
\newunicodechar{☆}{{\meiryo☆}}
\newunicodechar{●}{{\meiryo●}}
\newunicodechar{―}{{\meiryo―}}
\newunicodechar{①}{{\meiryo①}}
\newunicodechar{②}{{\meiryo②}}
\newunicodechar{③}{{\meiryo③}}
\newunicodechar{④}{{\meiryo④}}
\newunicodechar{μ}{{\greekfont μ}}

\makeatletter
\renewcommand{\@maketitle}{\begin{center}{\LARGE \@title \par}\end{center}}
\makeatother
